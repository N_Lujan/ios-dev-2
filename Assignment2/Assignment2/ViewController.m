//
//  ViewController.m
//  Assignment2
//
//  Created by Nicholas Lujan on 1/21/15.
//  Copyright (c) 2015 Nicholas Lujan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    player1Score = 0;
    player2Score = 0;
    
    UITextField* player1Txt = [[UITextField alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width/2, 100)];
    player1Txt.placeholder = @"Player 1 Name";
    player1Txt.textAlignment = NSTextAlignmentCenter;
    player1Txt.delegate = self;
    [self.view addSubview:player1Txt];
    
    player1Lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4 - 25, 350, 200, 100)];
    player1Lbl.text = [NSString stringWithFormat:@"%d", player1Score];
    player1Lbl.font = [UIFont systemFontOfSize:70];
    [self.view addSubview:player1Lbl];
    
    player1Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player1Lbl.frame.origin.x - 25, player1Lbl.frame.origin.y + player1Lbl.frame.size.height + 100, player1Lbl.frame.size.width, 50)];
    [player1Stepper addTarget:self action:@selector(player1StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:player1Stepper];
    
    
    UITextField* player2Txt = [[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, 50, self.view.frame.size.width/2, 100)];
    player2Txt.placeholder = @"Player 2 Name";
    player2Txt.textAlignment = NSTextAlignmentCenter;
    player2Txt.delegate = self;
    [self.view addSubview:player2Txt];
    
    player2Lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4 - 50 + 200, 350, 200, 100)];
    player2Lbl.text = [NSString stringWithFormat:@"%d", player2Score];
    player2Lbl.font = [UIFont systemFontOfSize:70];
    [self.view addSubview:player2Lbl];
    
    player2Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player2Lbl.frame.origin.x -25, player2Lbl.frame.origin.y + player2Lbl.frame.size.height + 100, player2Lbl.frame.size.width, 50)];
    [player2Stepper addTarget:self action:@selector(player2StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:player2Stepper];
    
    resetButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    resetButton.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
    [resetButton setTitle:@"RESET GAME" forState:(UIControlState)UIControlStateNormal];
    [resetButton addTarget:self action:@selector(resetScore) forControlEvents:(UIControlEvents)UIControlEventTouchUpInside];
    [self.view addSubview:resetButton];
}

-(void)player1StepperTouched:(UIStepper*)playerStepper {
    player1Lbl.text = [NSString stringWithFormat:@"%.f", playerStepper.value];
}

-(void)player2StepperTouched:(UIStepper*)playerStepper {
    player2Lbl.text = [NSString stringWithFormat:@"%.f", playerStepper.value];
}

-(void)resetScore {
    player1Lbl.text=@"0";
    player2Lbl.text=@"0";
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)resetBtnTouched:(id)sender {
    player1Lbl.text = @"0";
    player2Lbl.text = @"0";
    
    player1Stepper.value = 0;
    player2Stepper.value = 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
