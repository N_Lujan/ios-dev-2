//
//  AppDelegate.h
//  Assignment2
//
//  Created by Nicholas Lujan on 1/21/15.
//  Copyright (c) 2015 Nicholas Lujan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

