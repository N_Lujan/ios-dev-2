//
//  ViewController.h
//  Assignment2
//
//  Created by Nicholas Lujan on 1/21/15.
//  Copyright (c) 2015 Nicholas Lujan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
{
    int player2Score;
    int player1Score;
    
    UILabel* player1Lbl;
    UILabel* player2Lbl;
    
    UIStepper* player1Stepper;
    UIStepper* player2Stepper;
    
    UIButton *resetButton;
}
@end

